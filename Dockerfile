# Completez le Dockerfile afin de faire fonctionner le serveur Rails
FROM ruby:2.5


RUN apt-get update -qq && apt-get install -y nodejs postgresql-client
WORKDIR /tp-rails
COPY Gemfile /tp-rails/Gemfile
COPY Gemfile.lock /tp-rails/Gemfile.lock
RUN bundle lock --add-platform x86-mingw32 x86-mswin32 x64-mingw32 java
RUN bundle install
COPY . /tp-rails

# Conservez les lignes ci-dessous
COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]

EXPOSE 3000

# N'oubliez pas la commande pour démarrer le serveur

CMD ["rails", "server", "-b", "0.0.0.0"]
